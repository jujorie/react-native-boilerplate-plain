# React Native Boilerplate

Basic React Native Template

## Requirements

* [node ^10.0.0 / npm ^6.0.0](https://nodejs.org)
* [React Native ^0.60.0](https://facebook.github.io/react-native/)
  * [Android development](https://facebook.github.io/react-native/docs/getting-started.html#installing-dependencies-3)
  * [iOS development](https://facebook.github.io/react-native/docs/getting-started.html#installing-dependencies)

## NPM Commands

* [Running Apps](#running-apps)
* [Icons Workflow](#icon-workflow)
* [Renaming](#renaming)

### Renaming

To rename the projects. More info [here](https://www.npmjs.com/package/react-native-rename)

```sh
npm run rename
```

`WARNINIG`: You must do a backup previous use this

### Running Apps

#### Start Bundler

For just start the bundler

```sh
npm start
```

#### Running Android

For start bundler and the application

```sh
npm run android
```

For start just the application

```sh
npm run android:start
```

#### Running iOS

For start bundler and the application

```sh
npm run ios
```

For start just the application

```sh
npm run ios:start
```

### Icon workflow

Generate and manage icon using __[app-icon](https://github.com/dwmkerr/app-icon)__

#### Create Empty Icon

Create a blank icon on project root

```sh
npm run icon:init
```

#### Install Icons

Install the icons on the platforms

```sh
npm run icon:generate
```

## Platforms

### Android

* Only the first time you run the project, you need to generate a debug key with :

```sh
    cd android/app
    keytool -genkey -v -keystore debug.keystore -storepass android -alias androiddebugkey -keypass android -keyalg RSA -keysize 2048 -validity 10000`
    cd ../..
    npm run android
```

### iOS

* To prepare

```sh
    cd ios
    pod install
    cd ..
    npm start
    npm start ios
```
