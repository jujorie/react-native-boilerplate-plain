// For a detailed explanation regarding each configuration property, visit:
// https://jestjs.io/docs/en/configuration.html

module.exports = {
  'preset': 'react-native',
  'coverageReporters': [
    'text', 'html',
  ],
  'collectCoverage': true,
  'coverageThreshold': {
    'global': {
      'branches': 80,
      'functions': 80,
      'lines': 80,
      'statements': 80,
    },
  },
};
